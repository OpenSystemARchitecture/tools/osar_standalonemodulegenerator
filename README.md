# OSAR_StandaloneModuleGenerator

## General
Simple command line tool to generate an OSAR conform embedded software module.

## Abbreviations:
OSAR == Open System ARchitecture


## Useful Links:
- Overall OSAR-Artifactory: http://riddiks.ddns.net:8082/artifactory/webapp/#/artifacts/browse/tree/General/prj-open-system-architecture
- OSAR - ModuleCreator releases: http://riddiks.ddns.net:8082/artifactory/list/prj-open-system-architecture/Osar_Tools/OSAR_StandaloneModuleGenerator
- OSAR - Documents: http://riddiks.ddns.net:8082/artifactory/list/prj-open-system-architecture/Osar_Documents/