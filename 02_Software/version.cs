/******************************************************************************
 * @file      version.cs
 * @author  
 * @proj      
 * @date      Friday, December 11, 2020
 * @version   Application v. 1.0.0.4
 * @version   Generator   v. 1.2.3.9
 * @brief     Controls the assembly Version
 *****************************************************************************/
using System;
using System.Reflection;

[assembly: AssemblyVersion("1.0.0.4")]

namespace OsarStandaloneModuleGenerator
{
  static public class VersionClass
	{
		public static int major { get; set; }	 //Version of the programm
		public static int minor { get; set; }	 //Sub version of the programm
		public static int patch { get; set; }	 //Debug patch of the orgramme
		public static int build { get; set; }	 //Count programm builds
		
    static VersionClass()
    {
			major = 1;
			minor = 0;
			patch = 0;
			build = 4;
    }

		public static string getCurrentVersion()
    {
      return major.ToString() + '.' + minor.ToString() + '.' + patch.ToString() + '.' + build.ToString();
    }
	}
}
//!< @version 1.0.0	->	Inital version of the standalone generator
