﻿/*****************************************************************************************************************************
 * @file        Program.cs                                                                                                   *
 * @author      OSAR Team S.Reinemuth                                                                                        *
 * @date        16.08.2019                                                                                                   *
 * @brief       Implementation of an OSAR module standalone generator.                                                       *
 *                                                                                                                           *
 * @note        All OSAR code and programs are free software: you can redistribute it and/or modify it under the terms of    *
 *              the GNU General Public License as published by the Free Software Foundation, either version 3 of the         *
 *              License, or (at your option) any later version.                                                              *
 *                                                                                                                           *
 *              All code is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the       *
 *              implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public          *
 *              License for more details http://www.gnu.org/licenses/.                                                       *
 *                                                                                                                           *
*****************************************************************************************************************************/

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                     Used Name Spaces                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;
using OsarResources.Generator;

/****************************************************************************************************************************/
/* DO NOT CHANGE THIS COMMENT >>                    Active Name space                         << DO NOT CHANGE THIS COMMENT */
/****************************************************************************************************************************/
namespace OsarStandaloneModuleGenerator
{
  public struct ProgramArgs
  {
    public bool showHelp;
    public string configFilePath;
    public string moduleBaseFolderPath;
    public string moduleLibPath;
    public bool setDefaultCfg;
    public bool validateCfg;
    public bool generateCfg;
  }


  class Program
  {
    static ProgramArgs proArgs = new ProgramArgs();

    static void Main(string[] args)
    {
      // Init program arguments
      proArgs.moduleBaseFolderPath = "";
      proArgs.configFilePath = "";
      proArgs.moduleLibPath = "";
      proArgs.generateCfg = false;
      proArgs.setDefaultCfg = false;
      proArgs.showHelp = false;
      proArgs.validateCfg = false;

      if(args.Length > 0)
      {
        for (int idx = 0; idx < args.Length; idx++)
        {
          switch (args[idx])
          {
            //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            //++++++++++++++++++++++++++++++ Help Argument ++++++++++++++++++++++++++++++++
            //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            case "-h":
              proArgs.showHelp = true;
            break;

            //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            //++++++++++++++++++++++++++++ Default Cfg Argument +++++++++++++++++++++++++++
            //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            case "-d":
            proArgs.setDefaultCfg = true;
            break;

            //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            //+++++++++++++++++++++++++++ Generate Cfg Argument +++++++++++++++++++++++++++
            //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            case "-g":
            proArgs.generateCfg = true;
            break;

            //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            //+++++++++++++++++++++++++++ Validate Cfg Argument +++++++++++++++++++++++++++
            //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            case "-v":
            proArgs.validateCfg = true;
            break;

            //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            //+++++++++++++++++++++++++ Config file path Argument +++++++++++++++++++++++++
            //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            case "-c":
            {
              if (args.Length > idx)
              {
                proArgs.configFilePath = args[idx + 1];
                idx++;
              }
              else
              {
                Console.WriteLine("Invalid program arguments. Use -h to show help.");
              }
            }
            break;

            //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            //+++++++++++++++++++++++++ Module file path Argument +++++++++++++++++++++++++
            //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            case "-m":
            {
              if (args.Length > idx)
              {
                proArgs.moduleBaseFolderPath = args[idx + 1];
                idx++;
              }
              else
              {
                Console.WriteLine("Invalid program arguments. Use -h to show help.");
              }
            }
            break;

            //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            //++++++++++++++++++++++++ Module library path Argument +++++++++++++++++++++++
            //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            case "-l":
            {
              if (args.Length > idx)
              {
                proArgs.moduleLibPath = args[idx + 1];
                idx++;
              }
              else
              {
                Console.WriteLine("Invalid program arguments. Use -h to show help.");
              }
            }
            break;
          }
        }

        ExecuteArgs();
      }
      else
      {
        Console.WriteLine("Invalid program arguments. Use -h to show help.");
      }
    }

    static void ExecuteArgs()
    {
      GenInfoType genInfo, allGenInfos = new GenInfoType();
      if (true == proArgs.showHelp)
      {
        Console.WriteLine("Program to generate an OSAR module using the ModuleLibrary.");
        Console.WriteLine(" -h              Show help.");
        Console.WriteLine(" -d              Set default module configuration.");
        Console.WriteLine("                 Needs -c -m -l");
        Console.WriteLine(" -g              Generate module configuration.");
        Console.WriteLine("                 Needs -c -m -l");
        Console.WriteLine(" -v              Validate module configuration.");
        Console.WriteLine("                 Needs -c -m -l");
        Console.WriteLine(" -c \"path\"       Module configuration file path and name. Must be an absolute path");
        Console.WriteLine(" -m \"path\"       Module base folder path. Must be an absolute path");
        Console.WriteLine(" -l \"path\"       Module library path and name. Must be an absolute path");
      }
      else
      {
        File.Delete(proArgs.moduleBaseFolderPath + "\\01_Generator\\Generator_Info.txt");
        File.Delete(proArgs.moduleBaseFolderPath + "\\01_Generator\\Generator_Logs.txt");
        File.Delete(proArgs.moduleBaseFolderPath + "\\01_Generator\\Generator_Warnings.txt");
        File.Delete(proArgs.moduleBaseFolderPath + "\\01_Generator\\Generator_Errors.txt");

        if (( "" == proArgs.moduleBaseFolderPath ) || ( "" == proArgs.moduleLibPath ) || ( "" == proArgs.configFilePath ) ||
          (false == File.Exists(proArgs.moduleLibPath)))
        {
          Console.WriteLine("Invalid program arguments. Use -h to show help.");
        }
        else
        {
          var moduleLib = Assembly.LoadFrom(proArgs.moduleLibPath);
          Type moduleContextType = moduleLib.GetType("ModuleLibrary.ModuleContext");
          MethodInfo getInstanceMethode = moduleContextType.GetMethod("GetInstance");
          object obj = Activator.CreateInstance(moduleContextType);
          OsarModuleContextInterface moduleGen = (OsarModuleContextInterface)getInstanceMethode.Invoke(obj, null);

          if(true == proArgs.setDefaultCfg)
          {
            genInfo = moduleGen.GetViewModel(proArgs.configFilePath, proArgs.moduleBaseFolderPath).SetDefaultConfiguration();
            allGenInfos = new GenInfoType();
            allGenInfos.AddLogMsg(">>>>>>>>>> Set Default Configuration <<<<<<<<<<");
            allGenInfos = OsarGenHelper.MergeGenInfoType(allGenInfos, genInfo);
            storePrintouts(allGenInfos);
          }

          if (true == proArgs.validateCfg)
          {
            genInfo = moduleGen.GetViewModel(proArgs.configFilePath, proArgs.moduleBaseFolderPath).ValidateConfiguration();
            allGenInfos = new GenInfoType();
            allGenInfos.AddLogMsg(">>>>>>>>>> Validate Configuration <<<<<<<<<<");
            allGenInfos = OsarGenHelper.MergeGenInfoType(allGenInfos, genInfo);
            storePrintouts(allGenInfos);
          }

          if (true == proArgs.generateCfg)
          {
            genInfo = moduleGen.GetViewModel(proArgs.configFilePath, proArgs.moduleBaseFolderPath).GenerateConfiguration();
            allGenInfos = new GenInfoType();
            allGenInfos.AddLogMsg(">>>>>>>>>> Generate Configuration <<<<<<<<<<");
            allGenInfos = OsarGenHelper.MergeGenInfoType(allGenInfos, genInfo);
            storePrintouts(allGenInfos);
          }
        }
      }
    }

    static void storePrintouts(GenInfoType genInfo)
    {
      StreamWriter fileWriter = new StreamWriter(proArgs.moduleBaseFolderPath + "\\01_Generator\\Generator_Info.txt", true);
      foreach (string str in genInfo.info)
      {
        fileWriter.WriteLine(str);
        Console.WriteLine(str);
      }
      fileWriter.Close();

      fileWriter = new StreamWriter(proArgs.moduleBaseFolderPath + "\\01_Generator\\Generator_Logs.txt", true);
      foreach (string str in genInfo.log)
      {
        fileWriter.WriteLine(str);
        Console.WriteLine(str);
      }
      fileWriter.Close();

      fileWriter = new StreamWriter(proArgs.moduleBaseFolderPath + "\\01_Generator\\Generator_Warnings.txt", true);
      foreach (string str in genInfo.warning)
      {
        fileWriter.WriteLine(str);
        Console.WriteLine(str);
      }
      fileWriter.Close();

      fileWriter = new StreamWriter(proArgs.moduleBaseFolderPath + "\\01_Generator\\Generator_Errors.txt", true);
      foreach (string str in genInfo.error)
      {
        fileWriter.WriteLine(str);
        Console.WriteLine(str);
      }
      fileWriter.Close();
    }
  }
}
