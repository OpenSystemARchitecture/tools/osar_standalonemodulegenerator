pipeline {
  agent {
    node {
        label 'windows'
      }
    }

    environment {
        MSBuildToolsPath = 'C:\\Program Files (x86)\\Microsoft Visual Studio\\2019\\Community\\MSBuild\\Current\\Bin\\MSBuild.exe'
    }

    options {
      skipDefaultCheckout(true)
      gitLabConnection('GitLab Public Server')
    }

  stages {
/******************************************************************************************************************************/
/*                                             Start of checkout stage                                                        */
/******************************************************************************************************************************/
    stage('Checkout') {
      steps {
        echo 'Notify GitLab'
        updateGitlabCommitStatus name: 'Checkout', state: 'pending'

        echo 'Checkout git branch'
        cleanWs()
        checkout scm
      }
      post {
        success{
          updateGitlabCommitStatus name: 'Checkout', state: 'success'
        }
        failure {
          updateGitlabCommitStatus name: 'Checkout', state: 'failed'
        }
      }
    }

/******************************************************************************************************************************/
/*                                                 Start of build stage                                                       */
/******************************************************************************************************************************/
    stage('Build') {
      steps {
        dir('02_Software') {
          echo 'echo Building Visual Studio Solution File'
          updateGitlabCommitStatus name: 'Build', state: 'pending'

          bat '''
            "%MSBuildToolsPath%" OsarStandaloneModuleGenerator.sln
          '''
        }
      }
      post {
        success{
          updateGitlabCommitStatus name: 'Build', state: 'success'
        }
        failure {
          updateGitlabCommitStatus name: 'Build', state: 'failed'
        }
      }
    }

/******************************************************************************************************************************/
/*                                                  Start of test stage                                                       */
/******************************************************************************************************************************/
    // stage('Test') {
    //   steps {
    //     echo 'Testing not configured / available'
    //     updateGitlabCommitStatus name: 'Test', state: 'pending'
    //     //TODO: Add Test State
    //   }
    //   post {
    //     success{
    //       updateGitlabCommitStatus name: 'Test', state: 'success'
    //     }
    //     failure {
    //       updateGitlabCommitStatus name: 'Test', state: 'failed'
    //     }
    //   }
    // }

/******************************************************************************************************************************/
/*                                                  Collect Delivery                                                          */
/******************************************************************************************************************************/
    stage('Collect delivery data') {
      steps {
        echo 'Collect delivery data'
        updateGitlabCommitStatus name: 'Collect delivery data', state: 'pending'


        //>>>>> Create delivery folder <<<<<
        bat '''
          mkdir 99_Delivery
          mkdir 99_Delivery\\bin
        '''

        //>>>>> Collect build artefacts <<<<<
        bat '''
          copy 02_Software\\bin\\Debug\\OsarStandaloneModuleGenerator.exe 99_Delivery\\bin\\OsarStandaloneModuleGenerator.exe
          copy 02_Software\\bin\\Debug\\OsarStandaloneModuleGenerator.pdb 99_Delivery\\bin\\OsarStandaloneModuleGenerator.pdb
          copy 02_Software\\bin\\Debug\\OsarResources.dll 99_Delivery\\bin\\OsarResources.dll
        '''
      }
      post {
        success{
          updateGitlabCommitStatus name: 'Collect delivery data', state: 'success'
        }
        failure {
          updateGitlabCommitStatus name: 'Collect delivery data', state: 'failed'
        }
      }
    }

/******************************************************************************************************************************/
/*                                                Start of deploy stage                                                       */
/******************************************************************************************************************************/
    stage('Deploy') {
      steps {
        echo 'Start Deploying'
        updateGitlabCommitStatus name: 'Deploy', state: 'pending'

        script{
          String simpleBranchName = BRANCH_NAME.replace("%","").replace(" ","")
          println "simpleBranchName ${simpleBranchName}"

          withCredentials([usernamePassword(
            credentialsId: 'bca9b7d5-e21b-4205-8471-bf7dce5a432e',
            usernameVariable: 'USERNAME',
            passwordVariable: 'PASSWORD')])
          {
            def uploadSpec
            def buildInfo1
            def server = Artifactory.server 'Riddiks Artifactory Server'
            server.username = "${USERNAME}"
            server.password = "${PASSWORD}"

            if("master" == BRANCH_NAME)
            {
              uploadSpec = """{
              "files": [
                          {
                            "pattern": "99_Delivery/",
                            "recursive": "true",
                            "target": "prj-open-system-architecture/Osar_Tools/OSAR_StandaloneModuleGenerator/latest/"
                          }
                        ]}"""
              buildInfo1 = server.upload uploadSpec
              server.publishBuildInfo(buildInfo1)
            }
            else if(true == BRANCH_NAME.contains("release/"))
            {
              uploadSpec = """{
              "files": [
                          {
                            "pattern": "99_Delivery/",
                            "recursive": "true",
                            "target": "prj-open-system-architecture/Osar_Tools/OSAR_StandaloneModuleGenerator/${simpleBranchName}/"
                          }
                        ]}"""

              buildInfo1 = server.upload uploadSpec
              server.publishBuildInfo(buildInfo1)
            }
            else
            {
              println "Do not deploy for non master/release branch"
            }
          }
        }
      }
      post {
        success{
          updateGitlabCommitStatus name: 'Deploy', state: 'success'
        }
        failure {
          updateGitlabCommitStatus name: 'Deploy', state: 'failed'
        }
      }
    }
  }
}